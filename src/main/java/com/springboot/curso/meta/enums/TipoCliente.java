package com.springboot.curso.meta.enums;

public enum TipoCliente {
	PESSOA_FISICA(10, "Pessoa Física"),
	PESSOA_JURIDICA(20, "Pessoa Jurídica");
	
	private Integer codigo;
	private String descricao;
	
	private TipoCliente() {}

	private TipoCliente(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}
	
	public static TipoCliente toEnum(Integer codigo) {
		if (codigo == null)
			return null;
		
		for (TipoCliente cliente : TipoCliente.values()) {
			if (cliente.getCodigo().equals(codigo))
				return cliente;
		}
		
		throw new IllegalArgumentException("Id inválido: " + codigo);
	}
}
