package com.springboot.curso.meta.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.springboot.curso.meta.domain.Categoria;
import com.springboot.curso.meta.dto.CategoriaDTO;
import com.springboot.curso.meta.exceptions.ObjectNotFoundException;
import com.springboot.curso.meta.repositories.CategoriaRepository;

@Service
public class CategoriaService {
	
	@Autowired
	private CategoriaRepository repository;
	
	public Categoria findById(Integer id) {
		Optional<Categoria> categoria = repository.findById(id);
		
		return categoria.orElseThrow(
		  () -> new ObjectNotFoundException("Objeto não encontrado | Id: " + id + " Classe: " + CategoriaService.class.getName())
		);
	}
	
	public Categoria insert(Categoria categoria) {
		categoria.setId(null);
		return repository.save(categoria);
	}
	
	public List<Categoria> findAll() {
		return repository.findAll();
	}
	
	public Categoria update(Categoria categoria) {
		findById(categoria.getId());
		
		return repository.save(categoria);
	}
	
	public void delete(Integer id) {
		findById(id);
		repository.deleteById(id);
	}
	
	public Page<Categoria> findPage(Integer page, Integer rowsPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, rowsPage, Direction.valueOf(direction), orderBy);
		return repository.findAll(pageRequest);
	}
	
	public Categoria fromDTO(CategoriaDTO categoriaDTO) {
		return new Categoria(categoriaDTO.getId(), categoriaDTO.getNome());
	}
}
